//
//  GKVisitor.h
//  ios-interview
//
//  Created by Kwan Cheng on 3/29/17.
//  Copyright © 2017 Foursquare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GKVisitor : NSObject
@property NSString *identifier;
@property NSString *name;
@property NSInteger arriveTime;
@property NSInteger leaveTime;

-(id) initWithJson:(NSDictionary*)json;
+(GKVisitor*) noVisitorArrived:(NSInteger) arriveTime Left:(NSInteger) leaveTime;
@end
