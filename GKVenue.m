//
//  GKVenue.m
//  ios-interview
//
//  Created by Kwan Cheng on 3/29/17.
//  Copyright © 2017 Foursquare. All rights reserved.
//

#import "GKVenue.h"
#import "GKVisitor.h"

@implementation GKVenue
-(id) initWithJson:(NSDictionary*)json {
    if (self = [super init]) {
        _openTime = [[json valueForKey:@"openTime"] integerValue];
        _closeTime = [[json valueForKey:@"closeTime"] integerValue];
        
        NSArray *visitorsJson = (NSArray*)[json valueForKey:@"visitors"];
        NSMutableArray *tVisitors = [NSMutableArray array];
        for( NSDictionary* visitorJson in visitorsJson) {
            [tVisitors addObject:[[GKVisitor alloc] initWithJson:visitorJson]];
        }
        
        [tVisitors sortUsingComparator:^NSComparisonResult(GKVisitor* _Nonnull visitor1, GKVisitor*  _Nonnull visitor2) {
            NSInteger delta = visitor1.arriveTime - visitor2.arriveTime;
            if(delta == 0) return (NSComparisonResult)NSOrderedSame;
            
            return (delta < 0) ? (NSComparisonResult)NSOrderedAscending : (NSComparisonResult)NSOrderedDescending;
        }];
        _visitors = tVisitors;
        
        [self injectIdleTime];
    }
    return self;
}

/*
 O(N), no additional space requirements
 
 the visitors must be sorted by arrival time, which is done on line 24. Once
 sorted we can work our way forward without needing to track no visitors by
 arrivale time later in the array.
 
 The algorithm proceeds by keeping track of the latest contigues departure times.
 
 If a particular visitor has an arrival time that is after the "current" latest 
 departure time then we can be sure that there is gap, because of the sorting order
 we can be certain that there won't be other visitors "later" in the array that
 would have occupied that gap.
 
 If a gap is detected line 84, then we insert a no visitor visitor.
 
 Otherwise we check for max depature time.
 
 Finally we insert a no visitor between the closing time of the venue and the last visitor
 
 only tradeoff I made was that I was lazy in using a publicly accessible NSMutableArray.
 ideally it should be an NSArray
*/

-(void) injectIdleTime {
    if (_visitors.count == 0) return;
    
    NSInteger startAt = 0;
    
    GKVisitor *firstVisitor = [_visitors objectAtIndex:0];
    NSInteger delta = firstVisitor.arriveTime - self.openTime;
    if(delta != 0) {
        // no need to check for negative condition, as stated data is reliable
        GKVisitor *noVisitor = [GKVisitor noVisitorArrived:self.openTime Left:firstVisitor.arriveTime];
        [_visitors insertObject:noVisitor atIndex:0];
        startAt = 1;
    }

    NSInteger curIndex = startAt;
    NSInteger maxRangeIndex = startAt; // maximum occupied time
    
    while(curIndex < _visitors.count) {
        GKVisitor *curIndexVisitor = [_visitors objectAtIndex:curIndex];
        GKVisitor *maxRangeVisitor = [_visitors objectAtIndex:maxRangeIndex];
        
        // look for gap
        if ( curIndexVisitor.arriveTime > maxRangeVisitor.leaveTime ) {
            // insert noVisitor Gap
            GKVisitor *noVisitor = [GKVisitor noVisitorArrived:maxRangeVisitor.leaveTime Left:curIndexVisitor.arriveTime];
            [_visitors insertObject:noVisitor atIndex:curIndex];
            maxRangeIndex = curIndex + 1;
        } else if(curIndexVisitor.leaveTime > maxRangeVisitor.leaveTime) {
            maxRangeIndex = curIndex;
        }
        
        curIndex++;
    }
    
    // one last check
    GKVisitor *lastVisitor = [_visitors lastObject];
    delta = self.closeTime - lastVisitor.leaveTime;
    if(delta != 0) {
        // no need to check for negative condition, as stated, data is reliable
        GKVisitor *noVisitor = [GKVisitor noVisitorArrived:lastVisitor.leaveTime Left:self.closeTime];
        [_visitors addObject:noVisitor];
    }
}
@end
