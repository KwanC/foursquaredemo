//
//  FSQPeopleHereTableViewController.m
//  ios-interview
//
//  Created by Samuel Grossberg on 3/17/16.
//  Copyright © 2016 Foursquare. All rights reserved.
//

#import "FSQPeopleHereTableViewController.h"
#import "GKVenue.h"
#import "GKVisitor.h"

@interface FSQPeopleHereTableViewController () {
    GKVenue *venue;
}
@property (weak, nonatomic) IBOutlet UITableView *tableVisitors;
@end

@implementation FSQPeopleHereTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.tableVisitors.delegate = self;
    self.tableVisitors.dataSource = self;
    
    dispatch_queue_t loaderQueue = dispatch_queue_create("loaderQueue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(loaderQueue, ^{
        // no error checking was stated everything can be assumed to be perfect
        NSString *path = [[NSBundle mainBundle]pathForResource:@"people-here" ofType:@"json"];
        NSURL *url = [NSURL fileURLWithPath:path];
        
        NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:nil];
        
        NSDictionary *json = (NSMutableDictionary*)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        venue = [[GKVenue alloc]initWithJson:[json valueForKey:@"venue"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableVisitors reloadData];
        });
        
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (venue == nil) ? 0 : [venue.visitors count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil ) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }

    GKVisitor *visitor = [venue.visitors objectAtIndex:indexPath.row];
    
    NSDate *midnight = [[NSCalendar currentCalendar]startOfDayForDate: [NSDate date]]; // should really make this static
    NSDate *arriveDate = [midnight dateByAddingTimeInterval:visitor.arriveTime];
    NSDate *departureDate = [midnight dateByAddingTimeInterval:visitor.leaveTime];
    
    NSDateFormatter *df = [NSDateFormatter new];
    df.timeStyle = NSDateFormatterShortStyle;
    
    [cell.textLabel setText:visitor.name];
    [cell.detailTextLabel setText: [NSString stringWithFormat:@"%@ - %@", [df stringFromDate:arriveDate], [df stringFromDate:departureDate]]];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
