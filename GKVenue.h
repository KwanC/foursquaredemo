//
//  GKVenue.h
//  ios-interview
//
//  Created by Kwan Cheng on 3/29/17.
//  Copyright © 2017 Foursquare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GKVenue : NSObject
@property NSInteger openTime;
@property NSInteger closeTime;
@property NSMutableArray *visitors;

-(id) initWithJson:(NSDictionary*)json;
@end
