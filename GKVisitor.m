//
//  GKVisitor.m
//  ios-interview
//
//  Created by Kwan Cheng on 3/29/17.
//  Copyright © 2017 Foursquare. All rights reserved.
//

#import "GKVisitor.h"

@implementation GKVisitor
-(id) initWithJson:(NSDictionary*)json {
    if (self = [super init]) {
        _identifier = (NSString*)[json valueForKey:@"id"];
        _name = (NSString*)[json valueForKey:@"name"];
        _arriveTime = [[json valueForKey:@"arriveTime"]integerValue];
        _leaveTime = [[json valueForKey:@"leaveTime"]integerValue];
    }
    return self;
}

+(GKVisitor*) noVisitorArrived:(NSInteger) arriveTime Left:(NSInteger) leaveTime {
    GKVisitor *v = [[GKVisitor alloc]init];
    v.identifier = @"novisitor";
    v.name = @"No Visitor";
    v.arriveTime = arriveTime;
    v.leaveTime = leaveTime;
    return v;
}
@end
